# cif

A simple SESv4 client (written in standard Go) that can be statically linked, easily deployed and ran from anywhere.

## How To Build

```bash
make
```

## How to Install

```bash
sudo make install
```

## Get a list of bad domains

```bash
cif -get -indicator fqdn -confidence 8
```

## Submit an indicator
```bash
cif -submit -indicator 1.2.3.4 -tag your_tag_name
```

## Submit a list of indicators

```bash
while read ip; do cif -submit -indicator $ip -tag vt-ssh-failure; done < ssh-fails.txt
```

## Custom VT tags (auto submitted from Zeek logs)

  * vt-rdp-failure
  * vt-ssh-failure
  * vt-vnc-failure
  * vt-sip-scanner
  * vt-mysql-failure

## Notes

  * This implementation is purposefully small and simple to allow others to easily modify it and/or create their own SESv4 clients. Yet, the code is also robust and production ready (The VT ITSO uses a slightly modified version of this code as an AWS Lambda function to pull feeds in the cloud). 
  * The token is read from an environment variable which works well on stand-alone systems or in AWS Lambda functions. 
  * The code will work with most any version of [Go](https://golang.org/) on any OS (Windows, Linux, Mac)
  * The code does not require Docker or use any external libraries.
  * For questions about this code, please email btilley@gatech.edu
