package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"time"
)

type SESv4 struct {
	Data []struct {
		AdditionalData string   `json:"additional_data"`
		Altid          string   `json:"altid"`
		AltidTlp       string   `json:"altid_tlp"`
		Confidence     float64  `json:"confidence"`
		Count          int      `json:"count"`
		Description    string   `json:"description"`
		Firsttime      string   `json:"firsttime"`
		Group          []string `json:"group"`
		Indicator      string   `json:"indicator"`
		Itype          string   `json:"itype"`
		Lasttime       string   `json:"lasttime"`
		Provider       string   `json:"provider"`
		Reference      string   `json:"reference"`
		ReferenceTlp   string   `json:"reference_tlp"`
		Reporttime     string   `json:"reporttime"`
		Tags           []string `json:"tags"`
		Tlp            string   `json:"tlp"`
		UUID           string   `json:"uuid"`
	} `json:"data"`
}

func main() {
	var confidence = flag.String("confidence", "8.5", "the confidence level.")
	var get = flag.Bool("get", false, "get indicators.")
	var indicator = flag.String("indicator", "", "May be fqdn, ipv4, url, email, md5, etc.")
	var help = flag.Bool("help", false, "show help.")
	var limit = flag.String("limit", "50000", "limit get results.")
	var otime = flag.String("otime", "", "the time the indicator was observed. Use with -submit.")
	var submit = flag.Bool("submit", false, "submit an indicator. Requires -indicator.")
	var tag = flag.String("tag", "", "May be botnet, malware, hijacked, zeus, phishing, or scanner. Tags may also be combined (e.g. -tag botnet,malware,phishing). Use with -get or -submit and -indicator.")
	var tlp = flag.String("tlp", "privileged", "the tlp. Use with -submit and -indicator.")

	flag.Parse()

	if *help || len(os.Args) == 1 {
		flag.PrintDefaults()
		return
	}

	client := &http.Client{
		Timeout: 300 * time.Second,
	}

	if *get {
		params := url.Values{}
		params.Add("itype", *indicator)
		params.Add("tags", *tag)
		params.Add("confidence", *confidence)
		params.Add("nolog", "False")
		params.Add("limit", *limit)

		// Uncomment to view the full GET request
		//fmt.Printf("https://feeds.sesv4.ren-isac.net/feed?%v\n", params.Encode())

		req, err := http.NewRequest("GET", "https://feeds.sesv4.ren-isac.net/feed?"+params.Encode(), nil)
		if err != nil {
			log.Fatal(err)
		}

		req.Header.Set("Accept", "application/vnd.cif.v3+json")
		req.Header.Set("User-Agent", "vt-brad-cif/1.0.0")
		req.Header.Set("Authorization", "Token token="+os.Getenv("token"))
		req.Header.Set("Content-Type", "application/json")

		resp, err := client.Do(req)
		if err != nil {
			log.Fatal(err)
		}
		defer resp.Body.Close()

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Fatal(err)
		}

		// Uncomment to see raw data
		//fmt.Println(resp.Status)
		//fmt.Println(resp.Header)
		//fmt.Print(string(body))

		var ses SESv4
		err = json.Unmarshal(body, &ses)
		if err != nil {
			log.Println("error:", err)
		}

		for _, data := range ses.Data {
			fmt.Printf("%v\n", data.Indicator)
		}
	}

	if *submit && *indicator != "" {
		theURL := "https://feeds.sesv4.ren-isac.net/indicators"

		theTime := ""
		if *otime == "" {
			theTime = time.Now().UTC().Format("2006-01-02T15:04:05.999Z")
		} else {
			theTime = *otime
		}

		jsonData := fmt.Sprintf(
			`{"confidence":"%s","group":"ses_ops_community","indicator":"%s","tags":"%s","tlp":"%s","reporttime":"%s"}`,
			*confidence,
			*indicator,
			*tag,
			*tlp,
			theTime,
		)

		fmt.Printf("%v\n", jsonData)

		postReq, err := http.NewRequest("POST", theURL, bytes.NewBufferString(jsonData))
		if err != nil {
			log.Fatalln(err)
		}

		postReq.Header.Set("Accept", "application/vnd.cif.v3+json")
		postReq.Header.Set("User-Agent", "vt-brad-cif/1.0.0")
		postReq.Header.Set("Authorization", "Token token="+os.Getenv("wtoken"))
		postReq.Header.Set("Content-Type", "application/json")

		postResp, err := client.Do(postReq)
		if err != nil {
			log.Fatalln(err)
		}
		defer postResp.Body.Close()

		body, err := ioutil.ReadAll(postResp.Body)
		if err != nil {
			log.Fatalln(err)
		}

		// Uncomment to see raw data
		fmt.Println(postResp.Status)
		fmt.Println(postResp.Header)
		fmt.Println(string(body))
	}
}
