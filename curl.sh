#!/bin/bash

# Some basic SESv4 curl examples

# get indicators
curl --http1.1 -D - -H "Content-Type: application/json" -H "Authorization: Token token=$token" -X GET "https://feeds.sesv4.ren-isac.net/feed?itype=fqdn&tags=botnet%2Cmalware%2Chijacked%2Czeus%2Cphishing%2Cscanner&confidence=8.5&nolog=False&limit=5"

# submit an indicator
curl --http1.1 -D - -H "Content-Type: application/json" -H "Authorization: Token token=$wtoken" -X POST "https://feeds.sesv4.ren-isac.net/indicators" -d '{"indicator":"bad.example.com","group":"ses_ops_community","confidence":"5.7"}'
